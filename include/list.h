#ifndef _LIST_H
#define _LIST_H
list *create_list();
void insert_ready(list *ready_list, listobj *obj);
void insert_timer(list *timer_list, listobj *obj);
listobj *extract_first(list *generic_list);
listobj* extract_obj(listobj *obj);
#endif
