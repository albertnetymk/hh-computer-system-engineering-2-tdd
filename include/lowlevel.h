#ifndef _MEMORY_H
#define _MEMORY_H
void* malloc_isr(size_t size);
void free_isr(void *pointer);
void SaveContext();
void LoadContext();
void yield();
void timer0_start();
void remove_message(msg *message);
void isr_off();
#endif
