#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "kernel.h"
#include "lowlevel.h"
#include "list.h"
static void remove_blocked_message(mailbox *box, msg *message);
static void remove_unblocked_message(mailbox *box, msg *message);

uint tick_counter;
list *ready_list;
list *timer_list;
list *waiting_list;
TCB *running;

void idle()
{
    while(1);
}

exception init_kernel()
{
    tick_counter = 0;
    ready_list = create_list();
    if(!ready_list) {
        return FAIL;
    }
    timer_list = create_list();
    if(!timer_list) {
        return FAIL;
    }
    waiting_list = create_list();
    if(!waiting_list) {
        return FAIL;
    }
    return create_task(&idle, UINT_MAX);
}

exception create_task(void (*task_body)(), uint deadline)
{
    TCB *task = create_TCB(task_body, deadline);
    if (task == NULL) {
        return FAIL;
    }
    listobj *obj = obj_from_task(task);
    if (obj == NULL) {
        free_isr(task);
        return FAIL;
    }
    if (running != NULL) {
        isr_off();
        insert_ready(ready_list, obj);
        yield();
    } else {
        insert_ready(ready_list, obj);
    }
    return OK;
}

void run()
{
    timer0_start();
    running = ready_list->pHead->pNext->pTask;
    LoadContext();
}

void terminate()
{
    listobj *first = extract_first(ready_list);
    free_isr(first->pTask);
    free_isr(first);
    running = ready_list->pHead->pNext->pTask;
    LoadContext();
}

mailbox *create_mailbox(int nof_msg, int size_of_msg)
{
    mailbox *box = malloc_isr(sizeof(mailbox));
    if (box == NULL) {
        return NULL;
    }
    msg *head = malloc_isr(sizeof(msg));
    if (head == NULL) {
        free_isr(box);
        return NULL;
    }
    box->pHead = head;
    box->pTail = head;
    head->pNext = head;
    head->pPrevious = head;
    box->nMaxMessages = nof_msg;
    box->nDataSize = size_of_msg;
    return box;
}

static bool is_empty(mailbox *box)
{
    return box->pHead->pNext == box->pTail;
}

exception remove_mailbox(mailbox *box)
{
    if (is_empty(box)) {
        free_isr(box->pHead);
        free_isr(box);
        return OK;
    } else {
        return NOT_EMPTY;
    }
}

void move_to_waiting_list(listobj *obj)
{
    extract_obj(obj);
    listobj *head = waiting_list->pHead;
    obj->pNext = head->pNext;
    obj->pPrevious = head;
    head->pNext->pPrevious = obj;
    head->pNext = obj;
}

void append_to_mailbox(mailbox *box, msg *message)
{
    message->pPrevious = box->pTail->pPrevious;
    message->pNext = box->pTail;
    box->pTail->pPrevious->pNext = message;
    box->pTail->pPrevious = message;
}

exception send_wait(mailbox *box, void *data)
{
    int status = OK;
    isr_off();
    msg *message = box->pHead->pNext;
    if (!is_empty(box) && message->Status == RECEIVER ) {
        memcpy(box->pHead->pNext->pData, data, box->nDataSize);
        insert_ready(ready_list, extract_obj(message->pBlock));
        remove_blocked_message(box, message);
    } else {
        // assume there's room in this mailbox
        message = malloc_isr(sizeof(msg));
        if (message == NULL) {
            return FAIL;
        }
        message->Status = SENDER;
        message->pData = data;
        message->pBlock = ready_list->pHead->pNext;
        ready_list->pHead->pNext->pMessage = message;
        append_to_mailbox(box, message);
        box->nBlockedMsg++;
    }
    yield();
    if (tick_counter >= running->DeadLine) {
        message = ready_list->pHead->pNext->pMessage;
        remove_blocked_message(box, message);
        status = DEADLINE_REACHED;
    }
    return status;
}

exception receive_wait(mailbox *box, void *data)
{
    int status = OK;
    isr_off();
    msg *message = box->pHead->pNext;
    if (!is_empty(box) && message->Status == SENDER) {
        memcpy(data, message->pData, box->nDataSize);
        if (box->nMessages > 0) {
            remove_unblocked_message(box, message);
        } else {
            insert_ready(ready_list, extract_obj(message->pBlock));
            remove_blocked_message(box, message);
        }
    } else {
        // assume there's room in this mailbox
        message = malloc_isr(sizeof(msg));
        if (message == NULL) {
            return FAIL;
        }
        message->pData = data;
        message->Status = RECEIVER;
        message->pBlock = ready_list->pHead->pNext;
        ready_list->pHead->pNext->pMessage = message;
        append_to_mailbox(box, message);
        box->nBlockedMsg++;
    }
    yield();
    if (tick_counter >= running->DeadLine) {
        message = ready_list->pHead->pNext->pMessage;
        remove_blocked_message(box, message);
        status = DEADLINE_REACHED;
    }
    return status;
}

exception send_no_wait(mailbox *box, void *data)
{
    isr_off();
    msg *message = box->pHead->pNext;
    if (!is_empty(box) && message->Status == RECEIVER) {
        memcpy(message->pData, data, box->nDataSize);
        insert_ready(ready_list, extract_obj(message->pBlock));
        remove_blocked_message(box, message);
        yield();
    } else {
        if (box->nMessages == box->nMaxMessages) {
            remove_unblocked_message(box, box->pHead->pNext);
        }
        message = malloc_isr(sizeof(msg));
        if (message == NULL) {
            return FAIL;
        }
        void *pData = malloc_isr(box->nDataSize);
        if (pData == NULL) {
            free_isr(message);
            return FAIL;
        }
        memcpy(pData, data, box->nDataSize);
        message->Status = SENDER;
        message->pData = pData;
        message->pBlock = ready_list->pHead->pNext;
        ready_list->pHead->pNext->pMessage = message;
        append_to_mailbox(box, message);
        box->nMessages++;
    }
    return OK;
}

exception receive_no_wait(mailbox *box, void *data)
{
    isr_off();
    msg *message = box->pHead->pNext;
    if(!is_empty(box) && message->Status == SENDER) {
        memcpy(data, message->pData, box->nDataSize);
        if (box->nMessages > 0) {
            remove_unblocked_message(box, message);
        } else {
            insert_ready(ready_list, extract_obj(message->pBlock));
            remove_blocked_message(box, message);
        }
        yield();
        return OK;
    }
    return FAIL;
}

exception wait(uint tick)
{
    int status;
    isr_off();
    ready_list->pHead->pNext->nTCnt = tick_counter + tick;
    listobj *first = extract_first(ready_list);
    insert_timer(timer_list, first);
    yield();
    if (tick_counter >= running->DeadLine) {
        status = DEADLINE_REACHED;
    } else {
        status = OK;
    }
    return status;
}
void set_ticks(uint tick)
{
    tick_counter = tick;
}

uint ticks()
{
    return tick_counter;
}

uint deadline()
{
    return running->DeadLine;
}

void set_deadline(uint deadline)
{
    running->DeadLine = deadline;
}

void TimerInt()
{
    tick_counter++;
    listobj *current;
    current = timer_list->pHead->pNext;
    while(current != timer_list->pTail) {
        if (tick_counter >= current->nTCnt) {
            current = current->pNext;
            insert_ready(ready_list, extract_first(timer_list));
        } else {
            break;
        }
    }
    current = waiting_list->pHead->pNext;
    while(current != waiting_list->pTail) {
        if (tick_counter >= current->pTask->DeadLine) {
            current = current->pNext;
            insert_ready(ready_list, extract_first(waiting_list));
        } else {
            break;
        }
    }
    running = ready_list->pHead->pNext->pTask;
}

TCB *create_TCB(void (*body)(), uint deadline)
{
    TCB *task = malloc_isr(sizeof(TCB));
    if (task != NULL) {
        task->SP = &(task->StackSeg[STACK_SIZE-1]);
        task->PC = body;
        task->DeadLine = deadline;
        return task;
    }
    return NULL;
}

listobj* obj_from_task(TCB* task)
{
    listobj *obj = malloc_isr(sizeof(listobj));
    if (obj != NULL) {
        obj->pTask = task;
        obj->pMessage = NULL;
        return obj;
    }
    return NULL;
}

static void remove_blocked_message(mailbox *box, msg *message)
{
    message->pPrevious->pNext = message->pNext;
    message->pNext->pPrevious = message->pPrevious;
    free_isr(message);
    box->nBlockedMsg--;
}

static void remove_unblocked_message(mailbox *box, msg *message)
{
    message->pPrevious->pNext = message->pNext;
    message->pNext->pPrevious = message->pPrevious;
    free_isr(message->pData);
    free_isr(message);
    box->nMessages--;
}
