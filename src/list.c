#include <stdlib.h>
#include "kernel.h"
#include "lowlevel.h"
#include "list.h"
list *create_list()
{
    list *ret_list = malloc_isr(sizeof(list));
    if (ret_list == NULL) {
        return FAIL;
    }
    listobj *obj = malloc_isr(sizeof(listobj));
    if (obj == NULL) {
        free(ret_list);
        return FAIL;
    }
    ret_list->pHead = obj;
    ret_list->pTail = obj;
    obj->pNext = obj;
    obj->pPrevious = obj;
    return ret_list;
}

void insert_ready(list *ready_list, listobj *obj)
{
    listobj *current = ready_list->pHead->pNext;
    while(1) {
        if (current == ready_list->pTail) {
            break;
        }
        if (current->pTask->DeadLine > obj->pTask->DeadLine) {
            break;
        }
        current = current->pNext;
    }
    current = current->pPrevious;

    obj->pNext = current->pNext;
    obj->pPrevious = current;
    current->pNext->pPrevious = obj;
    current->pNext = obj;
}

void insert_timer(list *timer_list, listobj *obj)
{
    listobj *current = timer_list->pHead->pNext;
    while(1) {
        if (current == timer_list->pTail) {
            break;
        }
        if (current->nTCnt > obj->nTCnt) {
            break;
        }
        current = current->pNext;
    }
    current = current->pPrevious;

    obj->pNext = current->pNext;
    obj->pPrevious = current;
    current->pNext->pPrevious = obj;
    current->pNext = obj;
}

listobj *extract_first(list *generic_list)
{
    if (generic_list->pHead->pNext != generic_list->pTail) {
        listobj *obj = generic_list->pHead->pNext;
        generic_list->pHead->pNext = obj->pNext;
        obj->pNext->pPrevious = generic_list->pHead;
        return obj;
    }
    return NULL;
}

listobj* extract_obj(listobj *obj)
{
    obj->pPrevious->pNext = obj->pNext;
    obj->pNext->pPrevious = obj->pPrevious;
    return obj;
}

