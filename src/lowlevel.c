#include <stdio.h>
#include <stdlib.h>
#include "kernel.h"

extern TCB *running;
extern list *ready_list;
#ifdef TEST
uint malloc_left = 0;
uint free_counter = 0;


void* malloc_isr(size_t size)
{
    if (malloc_left > 0) {
        malloc_left--;
        return malloc(size);
    }
    return NULL;
}

void free_isr(void *pointer)
{
    free(pointer);
    free_counter++;
}

uint save_context_called;
uint load_context_called;

void SaveContext()
{
    save_context_called++;
}
void LoadContext()
{
    load_context_called++;
}
uint yield_called;
void yield()
{
    yield_called++;
}
uint timer_start_called = 0;
void timer0_start()
{
    timer_start_called++;
}
uint remove_message_called = 0;
void remove_message(msg *message)
{
    remove_message_called++;
}
uint isr_off_called = 0;
void isr_off()
{
    isr_off_called++;
}
#else
void* malloc_isr(size_t size)
{
    return malloc(size);
}

void free_isr(void *pointer)
{
    free(pointer);
}
void remove_message(msg *message)
{
    message->pPrevious->pNext = message->pNext;
    message->pNext->pPrevious = message->pPrevious;
    free_isr(message);
}

void yield()
{
    volatile bool first = TRUE;
    isr_off();
    SaveContext();
    if (first) {
        first = !first;
        running = ready_list->pHead->pNext->pTask;
        LoadContext();
    }
}

#endif
