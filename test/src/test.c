#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
static char *error_color = "\x1B[0;31m";
static char *success_color = "\x1B[0;32m";
static char *reset = "\x1B[0m";
void context(char *str)
{
    puts(str);
}
void ok(bool assertion, char *msg)
{
    if (!assertion) {
        printf("\t%sError: '%s' is unsatisfied.%s\n", error_color, msg, reset);
        exit(0);
    } else {
        printf("\t%sSuccess: %s.%s\n", success_color, msg, reset);
    }
}

void success()
{
    printf("\n%sCongratulations, you've passed all the tests!%s\n",
            success_color, reset);
}
