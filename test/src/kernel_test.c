#include <stdlib.h>
#include <stdio.h>
#include "kernel.h"
#include "list.h"
#include "test.h"
#define tick_counter tick_counter
#define ready_list ready_list
#define timer_list timer_list
#define waiting_list waiting_list

extern const uint malloc_left_minimal_for_create_list;

extern uint tick_counter;
extern list *ready_list;
extern list *timer_list;
extern list *waiting_list;
extern TCB *running;

extern void TimerInt();

extern uint malloc_left;
extern uint free_counter;
extern uint yield_called;
extern uint save_context_called;
extern uint load_context_called;
extern uint timer_start_called;
extern uint remove_message_called;
extern uint isr_off_called;

extern TCB *create_TCB(void (*body)(), uint deadline);
extern listobj* obj_from_task(TCB* task);
extern void idle();
extern void move_to_waiting_list(listobj *obj);
extern void append_to_mailbox(mailbox *box, msg *message);
void init_kernel_test()
{
    context("init_kernel_test");

    int malloc_lefts[] = {0, 2, 4, 6};
    int status;
    for (int i=0; i<sizeof(malloc_lefts)/sizeof(int); ++i) {
        malloc_left = malloc_lefts[i];
        status = init_kernel();
        ok(status == FAIL,
                "init_kernel returns FAIL for insufficient mem");
    }

    malloc_left = 8;
    status = init_kernel();
    ok(status == OK, "init_kernel returns OK for sufficient mem");

    tick_counter = 30;
    malloc_left = 8;
    init_kernel();
    ok(tick_counter == 0, "tick counter is set to 0");
    ok(ready_list != NULL, "ready list is initialized");
    ok(timer_list != NULL, "timer list is initialized");
    ok(waiting_list != NULL, "waiting list is initialized");
    listobj *obj = ready_list->pHead->pNext;
    ok(obj != ready_list->pTail,
            "ready list contains at least one task");
    ok(obj->pTask->PC == &idle, "idle task in in ready list");
}

void create_TCB_test()
{
    context("create_TCB_test");
    malloc_left = 0;
    ok(create_TCB(&create_TCB_test, 10) == NULL,
            "return NULL when there's no mem");
    malloc_left = 1;
    void *body = &create_TCB_test;
    uint deadline = 10;
    TCB *task = create_TCB(body, deadline);
    ok(task != NULL, "return sth when there's mem");
    ok(task->SP == &(task->StackSeg[STACK_SIZE-1]),
            "task has the right stack pointer");
    ok(task->PC == body, "task points to the right body");
    ok(task->DeadLine == deadline, "task has the right deadline");
}

void obj_from_task_test()
{
    context("obj_from_task_test");
    TCB *task;
    malloc_left = 0;
    task = create_TCB(&obj_from_task_test, 10);
    ok(obj_from_task(task) == NULL, "return NULL when there's no mem");
    malloc_left = 2;
    task = create_TCB(&obj_from_task_test, 10);
    listobj *obj = obj_from_task(task);
    ok(obj != NULL, "return sth when there's mem");
    ok(obj->pTask == task, "listobj points to the right task");
}

static list* setup_create_list()
{
    malloc_left = malloc_left_minimal_for_create_list;
    return create_list();
}

static void create_task_test_common(int status)
{
    ok(ready_list->pHead->pNext != ready_list->pTail,
            "ready list contains some elements");
    ok(ready_list->pHead->pNext->pTask->PC == &create_task_test_common,
            "the first element is pointing to the right function");
    ok(status == OK, "return OK when there's enough mem");
}

void create_task_test()
{
    context("create_task_test");
    malloc_left = 0;
    ok(create_task(create_task_test, 10) == FAIL,
            "return NULL when there's no mem for TCB");

    malloc_left = 1;
    free_counter = 0;
    ok(create_task(create_task_test, 10) == FAIL,
            "return NULL when there's no mem for listobj");
    ok(free_counter == 1, "free mem for TCB on failure case");

    ready_list = setup_create_list();
    malloc_left = 2;
    yield_called = 0;
    running = NULL;
    int status;
    status = create_task(create_task_test_common, 10);
    ok(yield_called == 0, "yield not called when kernel is not running");
    create_task_test_common(status);

    malloc_left = 2;
    yield_called = 0;
    isr_off_called = 0;
    running = malloc(sizeof(TCB));
    status = create_task(create_task_test_common, 10);
    ok(isr_off_called == 1, "isr off called once when kernel is running");
    ok(yield_called == 1, "yield called once when kernel is running");
    create_task_test_common(status);
}

static void setup_create_task(void (*body)(), uint deadline)
{
    malloc_left = 2;
    running = NULL;
    create_task(body, deadline);
}

void setup_ready_list_with_task(void (*body)())
{
    ready_list = setup_create_list();
    setup_create_task(body, 10);
}

void setup_ready_list_with_2_tasks(void (*t1)(), void (*t2)())
{
    ready_list = setup_create_list();
    setup_create_task(t1, 10);
    setup_create_task(t2, 10);
}

void run_test()
{
    context("run_test");
    void (*pc)() = &idle;
    setup_ready_list_with_task(pc);
    timer_start_called = 0;
    load_context_called = 0;
    run();
    ok(timer_start_called == 1, "timer_start called once");
    ok(running != NULL, "running is not null");
    ok(running->PC == pc, "running points to the first obj");
    ok(load_context_called == 1, "LoadContext is called once");
}

void terminate_test()
{
    context("terminate_test");
    setup_ready_list_with_2_tasks(&terminate_test, &idle);
    running = ready_list->pHead->pNext->pTask;
    free_counter = 0;
    load_context_called = 0;
    terminate();
    ok(free_counter == 2,
            "free is called twice, one for TCB, the other for listobj");
    ok(running->PC == &idle, "running is pointing to the next task");
    ok(load_context_called == 1, "LoadContext is called once");
}

void create_mailbox_test()
{
    context("create_mailbox_test");
    mailbox *box;
    malloc_left = 0;
    box = create_mailbox(1, 1);
    ok(box == NULL,
            "create_mailbox returns null when mem is not enough for box");
    malloc_left = 1;
    free_counter = 0;
    box = create_mailbox(1, 1);
    ok(box == NULL,
            "create_mailbox returns null when mem is not enough for head");
    ok(free_counter == 1, "free is called once if head is not created");

    malloc_left = 2;
    int nof_msg = 1;
    int size_of_msg = 1;
    box = create_mailbox(nof_msg, size_of_msg);
    ok(box->pHead != NULL, "mailbox head is initialized");
    ok(box->pTail == box->pHead, "mailbox head and tail is the same");
    ok(box->pHead->pNext == box->pHead, "head next is head");
    ok(box->pTail->pPrevious == box->pHead, "tail previous is head");
    ok(box->nMaxMessages == nof_msg, "mailbox size of msg is initialized");
    ok(box->nDataSize == size_of_msg,
            "mailbox size of msg is initialized");
    ok(box->nMessages == 0, "mailbox is empty after init");
    ok(box->nBlockedMsg == 0, "mailbox is empty after init");
}

static mailbox *setup_create_mailbox_with_one_msg(int nof_msg, int size)
{
    malloc_left = 2;
    mailbox *box = create_mailbox(nof_msg, size);
    msg *message = malloc(sizeof(msg));
    append_to_mailbox(box, message);
    return box;
}

void remove_mailbox_test()
{
    context("remove_mailbox_test");
    mailbox *box;
    malloc_left = 2;
    free_counter = 0;
    box = create_mailbox(1, 1);
    remove_mailbox(box);
    ok(free_counter == 2,
            "free is called twice, one for head, the other for head");

    malloc_left = 2;
    free_counter = 0;
    box = setup_create_mailbox_with_one_msg(1, 1);
    ok(remove_mailbox(box) == NOT_EMPTY,
            "return not empty when called with none empty mailbox");
}

static mailbox *setup_has_end(void *data, exception status)
{
    malloc_left = 2;
    mailbox *box = create_mailbox(1, 1);

    malloc_left = malloc_left_minimal_for_create_list;
    ready_list = create_list();

    malloc_left = malloc_left_minimal_for_create_list;
    waiting_list = create_list();

    running = NULL;

    malloc_left = 2;
    create_task(&idle, 5);
    malloc_left = 2;
    create_task(&idle, 10);

    listobj *receiver = ready_list->pHead->pNext;
    listobj *sender = receiver->pNext;

    msg *message = malloc(sizeof(msg));
    message->Status = status;
    message->pData = data;

    append_to_mailbox(box, message);

    receiver->pMessage = message;
    message->pBlock = receiver;
    move_to_waiting_list(receiver);
    running = sender->pTask;
    return box;
}

static mailbox *setup_no_end()
{
    malloc_left = 2;
    mailbox *box = create_mailbox(1, 1);

    malloc_left = malloc_left_minimal_for_create_list;
    ready_list = create_list();

    malloc_left = malloc_left_minimal_for_create_list;
    waiting_list = create_list();

    malloc_left = 2;
    running = NULL;
    create_task(&idle, 5);
    running = ready_list->pHead->pNext->pTask;
    return box;
}

static void send_wait_test_has_receiver()
{
    context("send_wait_test: with receiver");
    int status;
    char send_data = 'a';
    char data = 'b';
    char receive_data = 'b';
    mailbox *box = setup_has_end(&receive_data, RECEIVER);
    box->nBlockedMsg = 1;

    listobj *receiver = waiting_list->pHead->pNext;
    listobj *sender = ready_list->pHead->pNext;

    tick_counter = running->DeadLine-1;
    isr_off_called = 0;
    yield_called = 0;
    ok(box->nBlockedMsg == 1, "mailbox has one blocked email");
    status = send_wait(box, &send_data);
    ok(box->pHead->pNext == box->pTail, "mailbox is empty");
    ok(box->nBlockedMsg == 0, "the blocked email is removed");
    ok(isr_off_called == 1, "isr off called once");
    ok(receive_data == send_data, "data received successfully");
    ok(receiver->pPrevious == ready_list->pHead,
            "blocked task is moved to ready list");
    ok(yield_called == 1, "yield called once");
    ok(status == OK, "return OK for normal case");
}

static void send_wait_test_no_receiver()
{
    context("send_wait_test: without receiver");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 0;
        isr_off_called = 0;
        status = send_wait(box, &data);
        ok(isr_off_called == 1, "isr off called once");
        ok(status == FAIL, "return FAIL for insufficient mem");
    }

    context("send_wait_test: without receiver: finished on time");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 1;
        tick_counter = running->DeadLine-1;
        isr_off_called = 0;
        yield_called = 0;
        remove_message_called = 0;
        box->nBlockedMsg = 0;
        status = send_wait(box, &data);
        ok(box->pHead->pNext != box->pTail, "mailbox is not empty");
        ok(box->nBlockedMsg == 1, "mailbox has one blocked msg");
        msg *message = box->pHead->pNext;
        ok(box->pTail->pPrevious == message,
                "mess is at the end of mailbox");
        ok(message->Status == SENDER, "message has status SENDER");
        ok(message->pData == &data, "message has right data");
        ok(message->pBlock == sender, "message is pointing to the sender");
        ok(sender->pMessage == message, "listobj points back at message");
        ok(yield_called == 1, "yield called once");
        ok(status == OK, "return OK for enough mem");
    }

    context("send_wait_test: without receiver: deadline reached");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 1;
        tick_counter = running->DeadLine+1;
        isr_off_called = 0;
        yield_called = 0;

        status = send_wait(box, &data);

        ok(box->pHead->pNext == box->pTail, "mailbox is empty");
        ok(box->nBlockedMsg == 0, "mailbox has no msg");
        ok(yield_called == 1, "yield called once");
        ok(status == DEADLINE_REACHED,
                "return deadline reached for this case");
    }
}

void send_wait_test()
{
    context("send_wait_test");
    send_wait_test_has_receiver();
    send_wait_test_no_receiver();
}

static void receive_wait_test_has_sender()
{
    context("receive_wait_test: with sender");
    {
        int status;
        char send_data = 'a';
        char receive_data = 'b';
        mailbox *box = setup_has_end(&send_data, SENDER);

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        isr_off_called = 0;
        yield_called = 0;
        status = receive_wait(box, &receive_data);
        ok(isr_off_called == 1, "isr off called once");
        ok(yield_called == 1, "yield called once");
        ok(status == OK, "return OK for normal case");
    }

    context("receive_wait_test: with sender: blocked");
    {
        int status;
        char send_data = 'a';
        char receive_data = 'b';
        mailbox *box = setup_has_end(&send_data, SENDER);
        box->nBlockedMsg = 1;

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        status = receive_wait(box, &receive_data);
        ok(receive_data == send_data, "data transmitted successfully");
        ok(sender->pPrevious == ready_list->pHead,
                "blocked task is moved to ready list");
        ok(box->pHead->pNext == box->pTail, "mailbox is empty");
        ok(box->nBlockedMsg == 0, "msg counter is decremented");
        ok(status == OK, "return OK for normal case");
    }

    context("receive_wait_test: with sender: unblocked");
    {
        int status;
        char *send_data = malloc(sizeof(char));
        *send_data = 'a';
        char data = *send_data;
        char receive_data = 'b';
        mailbox *box = setup_has_end(send_data, SENDER);
        box->nBlockedMsg = 0;
        box->nMessages = 1;

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        free_counter = 0;
        status = receive_wait(box, &receive_data);
        ok(receive_data == data, "data transmitted successfully");
        ok(sender->pPrevious != ready_list->pHead,
                "unblock msg's pBlock is not touched");
        ok(box->nMessages == 0, "msg counter is decremented");
        ok(free_counter == 2, "both data and msg are released");
        ok(status == OK, "return OK for normal case");
    }
}

static void receive_wait_test_no_receiver()
{
    context("receive_wait_test: without receiver");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *receiver = ready_list->pHead->pNext;

        malloc_left = 0;
        tick_counter = running->DeadLine-1;
        status = receive_wait(box, &data);
        ok(status == FAIL, "return FAIL for insufficient mem");
    }
    context("receive_wait_test: without receiver: before deadline");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *receiver = ready_list->pHead->pNext;

        malloc_left = 1;
        tick_counter = running->DeadLine-1;
        yield_called = 0;
        status = receive_wait(box, &data);
        ok(box->pHead->pNext != box->pTail, "message is placed into mailbox");
        ok(box->nBlockedMsg == 1, "create one blocked msg");
        msg *message = box->pHead->pNext;
        ok(message->Status == RECEIVER, "message has status receiver");
        ok(message->pData == &data, "message has right data");
        ok(message->pBlock == receiver, "message is pointing to the receiver");
        ok(receiver->pMessage == message, "listobj points back at message");
        ok(yield_called == 1, "yield called once");
        ok(status == OK, "return OK for enough mem");
    }
    context("receive_wait_test: without receiver: miss deadline");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *receiver = ready_list->pHead->pNext;

        malloc_left = 1;
        tick_counter = running->DeadLine+1;
        yield_called = 0;
        status = receive_wait(box, &data);
        ok(box->pHead->pNext == box->pTail, "mailbox is empty");
        ok(box->nBlockedMsg == 0, "the blocked email is removed");
        ok(yield_called == 1, "yield called once");
        ok(status == DEADLINE_REACHED,
                "return deadline reached for this case");
    }
}

void receive_wait_test()
{
    context("receive_wait_test");
    receive_wait_test_has_sender();
    receive_wait_test_no_receiver();
}

static void send_no_wait_test_has_receiver()
{
    context("send_no_wait_test: with receiver");
    int status;
    char send_data = 'a';
    char data = 'b';
    char receive_data = 'b';
    mailbox *box = setup_has_end(&receive_data, RECEIVER);
    box->nBlockedMsg = 1;

    listobj *receiver = waiting_list->pHead->pNext;
    listobj *sender = ready_list->pHead->pNext;

    tick_counter = running->DeadLine-1;
    isr_off_called = 0;
    yield_called = 0;
    free_counter = 0;
    status = send_no_wait(box, &send_data);
    ok(isr_off_called == 1, "isr off called once");
    ok(receive_data == send_data, "data received successfully");
    ok(receiver->pPrevious == ready_list->pHead,
            "blocked task is moved to ready list");
    ok(box->pHead->pNext == box->pTail, "mailbox is empty");
    ok(box->nBlockedMsg == 0, "waiting message is removed");
    ok(yield_called == 1, "yield called once");
    ok(status == OK, "return OK for normal case");
}

static void send_no_wait_test_no_receiver()
{
    context("send_no_wait_test: without receiver: no mem1");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 0;
        isr_off_called = 0;
        status = send_no_wait(box, &data);
        ok(isr_off_called == 1, "isr off called once");
        ok(status == FAIL, "return FAIL for insufficient mem");
    }

    context("send_no_wait_test: without receiver: no mem2");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 1;
        isr_off_called = 0;
        free_counter = 0;
        status = send_no_wait(box, &data);
        ok(isr_off_called == 1, "isr off called once");
        ok(free_counter == 1,
                "message is freed if data could be created");
        ok(status == FAIL, "return FAIL for insufficient mem");
    }

    context("send_no_wait_test: without receiver: enough mem");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        listobj *sender = ready_list->pHead->pNext;

        malloc_left = 2;
        isr_off_called = 0;
        remove_message_called = 0;
        status = send_no_wait(box, &data);
        ok(box->pHead->pNext != box->pTail, "mailbox is not empty");
        ok(box->nMessages == 1, "mailbox has one unblocked msg");
        msg *message = box->pHead->pNext;
        ok(box->pTail->pPrevious == message,
                "message is at the end of mailbox");
        ok(message->Status == SENDER, "message has status SENDER");
        ok(message->pData != NULL, "message->pData is not null");
        ok(*((char*)message->pData) == data, "message has right data");
        ok(message->pBlock == sender, "message is pointing to the sender");
        ok(sender->pMessage == message, "listobj points back at message");
        ok(status == OK, "return OK for sufficient mem");
    }

    context("send_no_wait_test: without receiver: full mailbox");
    {
        char data = 'a';
        int status;
        mailbox *box = setup_no_end();
        box->nMessages = box->nMaxMessages;

        malloc_left = 2;
        status = send_no_wait(box, &data);
        ok(box->nMessages == box->nMaxMessages, "mailbox is still full");
        ok(status == OK, "return OK for sufficient mem");
    }
}

void send_no_wait_test()
{
    context("send_no_wait_test");
    send_no_wait_test_has_receiver();
    send_no_wait_test_no_receiver();
}

static void receive_no_wait_test_has_sender()
{
    context("receive_no_wait_test: with sender");
    {
        int status;
        char send_data = 'a';
        char receive_data = 'b';
        mailbox *box = setup_has_end(&send_data, SENDER);

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        isr_off_called = 0;
        yield_called = 0;
        status = receive_no_wait(box, &receive_data);
        ok(isr_off_called == 1, "isr off called once");
        ok(yield_called == 1, "yield called once");
        ok(status == OK, "return OK for normal case");
    }

    context("receive_wait_test: with sender: blocked");
    {
        int status;
        char send_data = 'a';
        char receive_data = 'b';
        mailbox *box = setup_has_end(&send_data, SENDER);
        box->nBlockedMsg = 1;

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        remove_message_called = 0;
        status = receive_no_wait(box, &receive_data);
        ok(receive_data == send_data, "data transmitted successfully");
        ok(sender->pPrevious == ready_list->pHead,
                "blocked task is moved to ready list");
        ok(box->pHead->pNext == box->pTail, "mailbox is empty");
        ok(box->nBlockedMsg == 0, "msg counter is decremented");
        ok(status == OK, "return OK for normal case");
    }

    context("receive_wait_test: with sender: unblocked");
    {
        int status;
        char *send_data = malloc(sizeof(char));
        *send_data = 'a';
        char data = *send_data;
        char receive_data = 'b';
        mailbox *box = setup_has_end(send_data, SENDER);
        box->nBlockedMsg = 0;
        box->nMessages = 1;

        listobj *sender = waiting_list->pHead->pNext;
        listobj *receiver = ready_list->pHead->pNext;

        tick_counter = running->DeadLine-1;
        status = receive_no_wait(box, &receive_data);
        ok(receive_data == data, "data transmitted successfully");
        ok(sender->pPrevious != ready_list->pHead,
                "unblock msg's pBlock is not touched");
        ok(box->pHead->pNext == box->pTail, "mailbox is empty");
        ok(box->nMessages == 0, "msg counter is decremented");
        ok(status == OK, "return OK for normal case");
    }
}

static void receive_no_wait_test_no_sender()
{
    int status;
    char data = 'b';
    char send_data = 'a';
    char receive_data = 'b';
    mailbox *box = setup_no_end();

    status = receive_no_wait(box, &data);
    ok(status == FAIL, "return FAIL if there's no one there");
}

void receive_no_wait_test()
{
    context("receive_no_wait_test");
    receive_no_wait_test_has_sender();
    receive_no_wait_test_no_sender();
}

void wait_test()
{
    context("wait_test");
    {
        setup_ready_list_with_task(&idle);
        timer_list = setup_create_list();
        listobj *first = ready_list->pHead->pNext;
        running = first->pTask;
        first->nTCnt = 0;
        uint tick = 5;
        tick_counter = 2;
        isr_off_called = 0;
        yield_called = 0;
        wait(tick);
        ok(isr_off_called == 1, "Interrupt is turned off");
        ok(yield_called == 1, "yield is called once");
        ok(first->nTCnt == tick_counter+tick, "waiting time is set correctly");
        ok(first->pPrevious == timer_list->pHead,
                "task is moved to timer list");
    }
    {
        setup_ready_list_with_task(&idle);
        timer_list = setup_create_list();
        listobj *first = ready_list->pHead->pNext;
        running = first->pTask;
        running->DeadLine = 2;
        first->nTCnt = 0;
        uint tick = 5;
        tick_counter = 2;
        int status = wait(tick);
        ok(status == DEADLINE_REACHED, "return deadline reached");
    }
    {
        setup_ready_list_with_task(&idle);
        timer_list = setup_create_list();
        listobj *first = ready_list->pHead->pNext;
        running = first->pTask;
        running->DeadLine = 3;
        first->nTCnt = 0;
        uint tick = 5;
        tick_counter = 2;
        int status = wait(tick);
        ok(status == OK, "return OK");
    }
}

void set_ticks_test()
{
    context("set_ticks_test");
    tick_counter = 5;
    set_ticks(10);
    ok(tick_counter == 10, "tick counter updates accordingly");
}

void ticks_test()
{
    context("ticks_test");
    tick_counter = 5;
    uint result = ticks();
    ok(result == tick_counter, "ticks return correct value");
}

void deadline_test()
{
    context("deadline_test");
    uint x = 10;
    malloc_left = 1;
    running = create_TCB(&idle, x);
    uint result = deadline();
    ok(result == x, "deadline returns the right deadline");
}

void set_deadline_test()
{
    context("set_deadline_test");
    uint x = 10;
    malloc_left = 1;
    running = create_TCB(&idle, 5);
    set_deadline(x);
    ok(running->DeadLine == x, "deadline is updated accordingly");
}

void TimerInt_test()
{
    context("TimerInt_test");
    tick_counter = 10;
    ready_list = setup_create_list();
    timer_list = setup_create_list();
    waiting_list = setup_create_list();
    setup_create_task(&idle, 5);
    setup_create_task(&idle, 10);
    setup_create_task(&idle, 15);
    setup_create_task(&idle, 20);
    listobj *first = ready_list->pHead->pNext;
    first->nTCnt = 5;
    listobj *second = first->pNext;
    listobj *third = second->pNext;
    third->nTCnt = 15;
    extract_obj(first);
    extract_obj(third);
    insert_timer(timer_list, first);
    insert_timer(timer_list, third);
    move_to_waiting_list(second);
    ok(second->pPrevious == waiting_list->pHead,
            "second task is the first in waiting list");
    TimerInt();
    ok(tick_counter == 11, "tick counter is incremented");
    ok(second->pPrevious == first,
            "second task is the second in ready list");
    ok(first->pPrevious == ready_list->pHead,
            "first task is the first in ready list");
    ok(third->pPrevious == timer_list->pHead,
            "third task is the first in timer list");

}
