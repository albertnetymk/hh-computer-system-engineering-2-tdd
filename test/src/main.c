#include <stdio.h>
#include <stdlib.h>
#include "kernel.h"
#include "list_test.h"
#include "kernel_test.h"

void list_test() {
    create_list_test();
    insert_ready_test();
    insert_timer_test();
    extract_first_test();
    extract_obj_test();
}
void kernel_test()
{
    init_kernel_test();
    create_TCB_test();
    obj_from_task_test();
    create_task_test();
    run_test();
    terminate_test();
    create_mailbox_test();
    remove_mailbox_test();
    send_wait_test();
    receive_wait_test();
    send_no_wait_test();
    receive_no_wait_test();
    wait_test();
    set_ticks_test();
    ticks_test();
    deadline_test();
    set_deadline_test();
    TimerInt_test();
}

int main() {
    list_test();
    kernel_test();
    // success();
    return 0;
}
