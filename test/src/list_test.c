#include <stdio.h>
#include <stdlib.h>
#include "kernel.h"
#include "list.h"
#include "test.h"
extern uint malloc_left;
const uint malloc_left_minimal_for_create_list = 3;
void create_list_test()
{
    context("create_list_test");
    malloc_left = 0;
    list *result;
    result = create_list();
    ok(result == FAIL, "FAIL is returned when list is not created");
    malloc_left = 1;
    result = create_list();
    ok(result == FAIL, "FAIL is returned when pHead is not created");
    malloc_left = 2;
    result = create_list();
    ok(result != NULL, "list is created when enough mem for list and phead");
    ok(result->pHead != NULL, "list has one valid head");
    ok(result->pTail != NULL, "list has one valid tail");
}

void insert_ready_test()
{
    context("insert_ready_test");
    malloc_left = malloc_left_minimal_for_create_list;
    list *ready_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for (int i=0; i<5; ++i) {
        listobj *obj = malloc(sizeof(listobj));
        TCB *task = malloc(sizeof(TCB));
        task->DeadLine = size - i;
        obj->pTask = task;
        objs[i] = obj;
        insert_ready(ready_list, obj);
    }
    char str[50];
    listobj *iterator = ready_list->pHead->pNext;
    for(int i=0; i<5; ++i) {
        sprintf(str, "%dth element is in the right position of ready list",
                i+1);
        ok(objs[size-i-1] == iterator, str);
        iterator = iterator->pNext;
    }
}

void insert_timer_test()
{
    context("insert_timer_test");
    malloc_left = malloc_left_minimal_for_create_list;
    list *timer_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<5; ++i) {
        listobj *obj = malloc(sizeof(listobj));
        obj->nTCnt = size - i;
        objs[i] = obj;
        insert_timer(timer_list, obj);
    }
    char str[50];
    listobj *iterator = timer_list->pHead->pNext;
    for(int i=0; i<5; ++i) {
        sprintf(str, "%dth element is in the right position of timer list",
                i+1);
        ok(objs[size-i-1] == iterator, str);
        iterator = iterator->pNext;
    }
}

void extract_first_test()
{
    context("extract_first_test");
    malloc_left = malloc_left_minimal_for_create_list;
    list *_list = create_list();
    listobj *objs[5];
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<size; ++i) {
        listobj *obj = malloc(sizeof(listobj));
        obj->nTCnt = i;
        objs[i] = obj;
        insert_timer(_list, obj);
    }
    ok(objs[0] == _list->pHead->pNext, "before: obj[0] in first position");
    listobj *first = extract_first(_list);
    ok(objs[1] == _list->pHead->pNext, "after: obj[1] in first position");
    ok(first == objs[0], "extract_first returns first obj");
}

void extract_obj_test()
{
    context("extract_obj_test");
    malloc_left = malloc_left_minimal_for_create_list;
    list *_list = create_list();
    listobj *objs[5];
    listobj *result;
    int size = sizeof(objs)/sizeof(listobj*);
    for(int i=0; i<size; ++i) {
        listobj *obj = malloc(sizeof(listobj));
        obj->nTCnt = i;
        objs[i] = obj;
        insert_timer(_list, obj);
    }
    ok(objs[1]->pNext == objs[2], "second obj'next is third obj");
    ok(objs[3]->pPrevious == objs[2], "forth obj's previous is third obj");
    result = extract_obj(objs[2]);
    ok(objs[1]->pNext == objs[3], "second obj'next is forth obj");
    ok(objs[3]->pPrevious == objs[1], "forth obj's previous is second obj");
    ok(result == objs[2], "returns extracted listobj");
}
