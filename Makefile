ifeq (,$(filter obj, $(notdir $(CURDIR))))
OBJDIR := obj
MAKETARGET = $(MAKE) $(MFLAGS) --no-print-directory -C $@ -f ../Makefile \
			      ROOT_DIR=.. $(MAKECMDGOALS)

.PHONY: $(OBJDIR)
$(OBJDIR):
	@$(MAKETARGET)

Makefile : ;

.PHONY: clean
clean:
	@rm -rf $(OBJDIR)/*

% : $(OBJDIR);
else
define make-depend
    $(CC) -MM $(CPPFLAGS) $1 | \
    sed 's,$(notdir $2) *:,$2 $3 : ,' > $3.tmp
    mv $3.tmp $3
endef
define make-depend-test
    $(CC) -MM $(CPPFLAGS) $1 | \
    sed 's,$(subst -test,,$2) *:,$2 $3 : ,' > $3.tmp
    mv $3.tmp $3
endef

COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c
%.o: %.c
	$(call make-depend,$<,$@,$(subst .o,.d,$@))
	$(COMPILE.c) $(OUTPUT_OPTION) $<
%-test.o: %.c
	$(call make-depend-test,$<,$@,$(subst .o,.d,$@))
	$(COMPILE.c) -DTEST $(OUTPUT_OPTION) $< -o $@

CPPFLAGS += -I$(ROOT_DIR)/include -I$(ROOT_DIR)/test/include
CC = clang

TARGET = target

# SOURCES += kernel.c
# SOURCES += kernel-test.c
# SOURCES += list.c
# SOURCES += memory.c


# SOURCESTEST += list_test.c
# SOURCESTEST += main.c
# SOURCESTEST += test.c

SOURCES += $(notdir $(wildcard $(ROOT_DIR)/src/*.c))
SOURCESTEST += $(notdir $(wildcard $(ROOT_DIR)/test/src/*.c))

HEADERS = $(notdir $(wildcard $(ROOT_DIR)/include/*.h))
HEADERSTEST = $(notdir $(wildcard $(ROOT_DIR)/test/include/*.h))

OBJECTS = $(subst .s,.o, $(subst .c,.o,$(SOURCES)))
OBJECTSTEST = $(subst .c,-test.o, $(SOURCESTEST) $(SOURCES))
DEPENDENCIES = $(subst .c,.d, $(filter %.c, $(SOURCES)))
DEPENDENCIES += $(subst .o,.d,$(OBJECTSTEST))


vpath %.c $(ROOT_DIR)/src
vpath %.c $(ROOT_DIR)/test/src

default: test-run
# default: compile

.PHONY: test
test: $(OBJECTSTEST)
	@$(CC) $^ $(LOADLIBES) $(LDLIBS) -o $@

.PHONY: test-run
test-run: test
	@./test

all: $(TARGET);

.PHONY: compile
compile: $(OBJECTS) ;

$(TARGET): $(OBJECTS)
	@$(CC) $^ $(LOADLIBES) $(LDLIBS) -o $@

$(ROOT_DIR)/Makefile: ;
$(OBJECTS) $(OBJECTSTEST): %.o: $(ROOT_DIR)/Makefile
ifneq "$(strip $(MAKECMDGOALS))" "clean"
-include $(DEPENDENCIES)
endif

endif
